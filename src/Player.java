import java.util.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
public class Player {

    static class Buffer {
        LinkedList<Cell> buffer;

        Buffer() {
            buffer = new LinkedList<>();
        }

        Cell draw() {
            return buffer.removeLast();
        }

        void add(Cell cell) {
            buffer.add(cell);
        }

        boolean isEmpty() {
            return buffer.isEmpty();
        }
    }

    static class Cell {
        int x;
        int y;
        char mark;
        Cell preview;
        boolean path = false;
        boolean seen = false;

        public Cell(int x, int y) {
            this.x = x;
            this.y = y;
        }

        boolean isUnknown() {
            return mark == '?';
        }

        boolean isWall() {
            return mark == '#';
        }

        boolean isSpace() {
            return mark == '.';
        }

        boolean isStart() {
            return mark == 'T';
        }

        boolean isExit() {
            return mark == 'C';
        }

        boolean isSeen() {
            return seen;
        }

        void markSeen() {
            this.seen = true;
        }

        boolean isPath() {
            return path;
        }

        void markPath() {
            this.path = true;
        }

        void hasPreview(Cell cell) {
            this.preview = cell;
        }

        String printPositionWith(Cell cell) {
            if (cell == null) {
                return "";
            }
            String direction = "";
            if (x > cell.x) {
                System.out.println(this);
                direction = "UP";
            } else if (x < cell.x) {
                direction = "DOWN";
            } else if (y > cell.y) {
                direction = "LEFT";
            } else if (y < cell.y) {
                direction = "RIGHT";
            }
            return direction;
        }

        @Override
        public String toString() {
            return mark + "(" + x + ", " + y + ")";
        }
    }

    static class Labyrinth {
        int rows;
        int columns;
        Cell[][] cells;
        Cell kirk;
        Cell start;
        Cell exit;

        Labyrinth(int rows, int columns) {
            this.rows = rows;
            this.columns = columns;
            initializeCells(rows, columns);
        }

        private void initializeCells(int rows, int columns) {
            this.cells = new Cell[rows][columns];
            for (int row = 0; row < rows; row++) {
                for (int column = 0; column < columns; column++) {
                    cells[row][column] = new Cell(row, column);
                }
            }
        }

        boolean cellIsOut(int row, int column) {
            return (row < 0 && row >= rows) && (column < 0 && column >= columns);
        }

        Cell getCell(int row, int column) {
            if (cellIsOut(row, column)) {
                return null;
            }
            return cells[row][column];
        }

        LinkedList<Cell> getNeighbours() {
            LinkedList<Cell> neighbours = new LinkedList<>();
            int row = kirk.x;
            int column = kirk.y;
            int[][] dd = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}};
            for (int i = 0; i < dd.length; i++) {
                int dx = dd[i][0];
                int dy = dd[i][1];
                int x = row + dx;
                int y = column + dy;
                Cell neighbour = getCell(x, y);
                if (neighbour != null && !neighbour.isWall() && !neighbour.isUnknown()) {
                    neighbours.add(neighbour);
                }
            }
            return neighbours;
        }

        void goBack() {
            Cell cell = exit;
            Cell prev = exit.preview;
            while (cell != null) {
                String backPath = cell.printPositionWith(prev);
                if (!backPath.isEmpty()) {
                    System.out.println(backPath);
                }
                Cell tmp = prev;
                prev = cell.preview;
                cell = tmp;
            }
        }

        public void resetSeenCells() {
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    cells[i][j].seen = false;
                }
            }
        }
    }

    static void explore(Labyrinth labyrinth) {
        Cell start = labyrinth.start;
        labyrinth.kirk = start;
        start.markSeen();
        Buffer buffer = new Buffer();
        buffer.add(start);
        while (!buffer.isEmpty()) {
            Cell cell = buffer.draw();
            String direction = labyrinth.kirk.printPositionWith(cell);
            if (!direction.isEmpty()) {
                System.out.println(direction);
            }
            labyrinth.kirk = cell;
            if (labyrinth.kirk.isExit()) {
                labyrinth.exit = labyrinth.kirk;
                System.err.println("Got exit " + labyrinth.exit);
                return;
            }
            LinkedList<Cell> neighbours = labyrinth.getNeighbours();
            //System.err.println(neighbours);
            for (Cell neighbour : neighbours) {
                if (!neighbour.isSeen()) {
                    neighbour.markSeen();
                    neighbour.hasPreview(cell);
                    buffer.add(neighbour);
                }
            }
            //cell.markSeen();
        }
    }
    static void buildPath(Labyrinth labyrinth) {
        Cell start = labyrinth.start;
        labyrinth.kirk = start;
        start.markSeen();
        Buffer buffer = new Buffer();
        buffer.add(start);
        while (!buffer.isEmpty()) {
            Cell cell = buffer.draw();
            labyrinth.kirk = cell;
            if (labyrinth.kirk.isExit()) {
                labyrinth.exit = labyrinth.kirk;
                return;
            }
            LinkedList<Cell> neighbours = labyrinth.getNeighbours();
            for (Cell neighbour : neighbours) {
                if (!neighbour.isSeen()) {
                    neighbour.markSeen();
                    //neighbour.hasPreview(cell);
                    cell.markPath();
                    buffer.add(neighbour);
                    if (neighbour.isExit()) {
                        System.err.println("Got exit " + neighbour);
                    }
                }
            }
        }
    }

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int R = in.nextInt(); // number of rows.
        int C = in.nextInt(); // number of columns.
        int A = in.nextInt(); // number of rounds between the time the alarm countdown is activated and the time the alarm goes off.
        Labyrinth labyrinth = new Labyrinth(R, C);
        System.err.println(R + " " + C + " " + A);
        // game loop
        while (true) {
            int KR = in.nextInt(); // x where Kirk is located.
            int KC = in.nextInt(); // y where Kirk is located.
            labyrinth.kirk = labyrinth.cells[KR][KC];
            labyrinth.start = labyrinth.kirk;
            System.err.println(KR + " "  + KC);
            for (int i = 0; i < R; i++) {
                String ROW = in.next(); // C of the characters in '#.TC?' (i.e. one line of the ASCII maze).
                for (int j = 0; j < ROW.length(); j++) {
                    labyrinth.cells[i][j].mark = ROW.charAt(j);
                }
                //System.err.println(ROW);
            }
            System.err.println("--------------");
            explore(labyrinth);
            if (labyrinth.exit != null) {
                labyrinth.resetSeenCells();
                buildPath(labyrinth);
                System.err.println("Going back...");
                labyrinth.goBack();
            }
            Arrays.sort(new Double[]{}, new Comparator<Double>() {
                @Override
                public int compare(Double o1, Double o2) {
                    return 0;
                }
            });
        }
    }
}